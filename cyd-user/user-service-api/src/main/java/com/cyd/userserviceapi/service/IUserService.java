package com.cyd.userserviceapi.service;

import com.cyd.userserviceapi.service.VO.TUserVO;

/**
 * @author  cyd
 * @date 2020/02/15
 */
public interface IUserService {

    /**
     * 根据用户id获取用户信息
     * @param id
     * @return
     */
    public TUserVO getUserById(int id);
}

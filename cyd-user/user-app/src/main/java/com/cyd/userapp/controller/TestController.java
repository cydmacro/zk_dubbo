package com.cyd.userapp.controller;


import com.cyd.userserviceapi.service.VO.TUserVO;
import com.cyd.userapp.service.dubbo.UserService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
public class TestController {

    @Resource
    private UserService userService;

   @RequestMapping("/test")
    public TUserVO getUserById(Integer id){
       if(id!=null && id.equals("")){
           return  new TUserVO();
       }else {
           return userService.getUserById(id);
       }
    }



}

package com.cyd.userapp;

import org.apache.dubbo.config.spring.context.annotation.DubboComponentScan;
import org.apache.dubbo.config.spring.context.annotation.EnableDubboConfig;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableDubboConfig
@DubboComponentScan({"com.cyd.userapp.service.dubbo","com.cyd.couponapp.service.dubbo"})
@SpringBootApplication
@MapperScan("com.cyd.userapp.mapper")
@EnableScheduling
public class UserAppApplication {

    public static void main(String[] args) {
        SpringApplication.run(UserAppApplication.class, args);

        Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("执行jvm  shutdownhook");
            }
        }));
    }


}

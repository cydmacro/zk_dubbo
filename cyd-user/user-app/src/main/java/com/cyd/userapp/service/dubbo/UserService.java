package com.cyd.userapp.service.dubbo;

import com.cyd.userapp.domain.TUserExample;
import com.cyd.userserviceapi.service.IUserService;
import com.cyd.userserviceapi.service.VO.TUserVO;
import com.cyd.userapp.domain.TUser;
import com.cyd.userapp.mapper.TUserMapper;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.Lists;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.BeanUtils;

import javax.annotation.Resource;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

/**
 * @author  cyd
 * @date 2019/11/15
 */
@Service
public class UserService  implements IUserService {

    @Resource
    private TUserMapper tUserMapper;

    LoadingCache<Integer, TUser> loadUserCache=CacheBuilder.newBuilder()
            .expireAfterWrite(10, TimeUnit.MINUTES)
            .refreshAfterWrite(5, TimeUnit.MINUTES)
            .build(new CacheLoader<Integer, TUser>() {
                @Override
                public TUser load(Integer key) throws Exception {
                    return loadUser(key);
                }
            });

    private TUser loadUser(Integer key) {
//        TUser tuser=tUserMapper.selectByPrimaryKey(id);
//        TUserVO userVO=new TUserVO();
//        BeanUtils.copyProperties(tuser,userVO);
//        TUserExample example=new TUserExample();
//        example.createCriteria().andIdEqualTo(key);
        return  tUserMapper.selectByPrimaryKey(key);
    }

    public void print(){
        System.err.println("enter coupon service");
    }


/**
 * 通过用户ID获取用户信息
 * @param id
 * @return
 */

 @Override
 public TUserVO getUserById(int id){
     TUserVO userVO=new TUserVO();
     try {
         TUser tuser=loadUserCache.get(1);
         BeanUtils.copyProperties(tuser,userVO);
     } catch (ExecutionException e) {
         e.printStackTrace();
     }
     return  userVO;
 }

}

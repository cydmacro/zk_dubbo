package com.cyd.couponapp.service.schedule;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
public class HelloSchedule {
    private static final Logger logger = LoggerFactory.getLogger(HelloSchedule.class);

//    @Scheduled(cron="0/10 * * * * ?")//10秒  linux unit rsync cron
    public  void hello(){
        logger.info("enter hello world");
    }
}

package com.cyd.couponapp.controller;


import com.cyd.couponapp.domain.TCoupon;
import com.cyd.couponapp.domain.TCouponExample;
import com.cyd.couponapp.mapper.TCouponMapper;
import com.cyd.couponapp.service.dubbo.CouponService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@RestController
public class TestController {

    @Resource
    private TCouponMapper tCouponMapper;

    @Resource
    private CouponService couponService;


   @RequestMapping("/test")
    public String test(){
       TCouponExample example = new TCouponExample();
       example.createCriteria().andCodeEqualTo("9510536e-1d68-4b3b-b7c9-9923503423f2").andStatusEqualTo(0)
               .andAchieveAmountBetween(100,1000).andTitleNotLike("111");
       List<TCoupon> tCoupon =  tCouponMapper.selectByExample(example);
       System.err.println(tCoupon);
        return  tCoupon.toString();
    }

    @RequestMapping("/testQuery")
    public String testQuery(){
       return  couponService.query();
    }

    @RequestMapping("/testUserApi")
    public String getUserById(int id){
       return couponService.getUserById(id);
    }

    @RequestMapping("/getCouponList")
    public String  getCouponList()  {
        return couponService.getCouponList().toString();
    }

    @RequestMapping("/getCouponListByIds")
   public String  getCouponListByIds(String ids)  {
        return couponService.getCouponListByIds(ids).toString();
    }
}

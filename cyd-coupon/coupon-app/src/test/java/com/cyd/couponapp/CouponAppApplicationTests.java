//package com.cyd.couponapp;
//
//import com.alibaba.fastjson.JSON;
//import com.cyd.couponapp.domain.TCoupon;
//import com.cyd.couponapp.domain.TCouponExample;
//import com.cyd.couponapp.mapper.TCouponMapper;
//import com.cyd.couponapp.service.dubbo.CouponService;
//
//import com.cyd.couponserviceapi.dto.CouponDto;
//import com.cyd.couponserviceapi.dto.UserCouponDto;
//import jdk.nashorn.internal.ir.annotations.Reference;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.data.redis.core.RedisTemplate;
//import org.springframework.test.context.junit4.SpringRunner;
//
//import javax.annotation.Resource;
//import java.util.Date;
//import java.util.List;
//import java.util.UUID;
//
//@RunWith(SpringRunner.class)
//@SpringBootTest(classes = CouponAppApplication.class)
//public class CouponAppApplicationTests {
//
//    /*private static final Logger log = LoggerFactory.getLogger(CouponAppApplicationTests.class);
//    @Resource
//    private CouponService couponService;
//
//    @Resource
//    private TCouponMapper tCouponMapper;
//
//    @Resource
//    private RedisTemplate redisTemplate;
//
//    @Test
//    public void contextLoads() {
//        System.err.println("hello world");
//    }
//
//    @Test
//    public void insert(){
//        for(int i=0;i<200000;i++){
//            TCoupon tCoupon = new TCoupon();
//            tCoupon.setAchieveAmount(500);
//            tCoupon.setReduceAmount(20);
//            tCoupon.setCreatetime(new Date());
//            tCoupon.setCode(UUID.randomUUID().toString());
//            tCoupon.setPicUrl("1.jpg");
//            tCoupon.setStatus(0);
//            tCoupon.setStock(100L);
//            tCoupon.setTitle("测试coupon");
//            tCouponMapper.insert(tCoupon);
//        }
//
//    }
//
//    @Test
//    public void delete(){
//        tCouponMapper.deleteByPrimaryKey(101120);
//    }
//
//    @Test
//    public void update(){
//        TCoupon tCoupon = new TCoupon();
//        tCoupon.setId(101119);
//        tCoupon.setCode("111111");
//        tCouponMapper.updateByPrimaryKeySelective(tCoupon);
//        tCouponMapper.updateByPrimaryKey(tCoupon);
//    }
//
//
//    @Test
//    public void select(){
//        // select * from t_coupon where code = "00415d96-49bd-4cce-83e3-08302b9aa084" and status=0 and achieve_amount between (100,1000) and title not like '%111%';
//        TCouponExample example = new TCouponExample();
//        example.createCriteria().andCodeEqualTo("b5f81687-185f-4484-9e6b-89a687a0a72f").andStatusEqualTo(0)
//                .andAchieveAmountBetween(100,1000).andTitleNotLike("111");
//        List<TCoupon> tCoupon =  tCouponMapper.selectByExample(example);
//        System.err.println(tCoupon);
//    }
//
//   @Test
//    public void testQuery(){
//        List<CouponDto> list=couponService.getCouponList();
//        System.out.println(list);
//    }
//
//    @Test
//    public void testSaveUserCoupon(){
//        UserCouponDto dto=new UserCouponDto();
//        dto.setCouponId(101126);
//        dto.setUserId(1);
//        dto.setOrderId(1);
//        couponService.saveUserCoupon(dto);
//        log.info("用户获取优惠券测试");
//    }
//
//    @Test
//    public void testUserCouponList(){
//        System.out.println(JSON.toJSONString(couponService.userCouponList(1)));
//    }
//
//    @Test
//    public void testRedis(){
//        redisTemplate.opsForValue().set("name","cyd");
//        System.out.println(redisTemplate.opsForValue().get("name"));
//    }
//
//    @Test
//    public void testRedisSortSet(){
//        redisTemplate.opsForZSet().add("myset4","four",4);
//        redisTemplate.opsForZSet().add("myset4","five",5);
//        redisTemplate.opsForZSet().add("myset4","siex",6);
//        redisTemplate.opsForZSet().add("myset4","seven",7);
//        redisTemplate.opsForZSet().add("myset4","eight",8);
//        redisTemplate.opsForZSet().add("myset4","nine",9);
//        redisTemplate.opsForZSet().add("myset4","ten",10);
//        redisTemplate.opsForZSet().remove("myset","ten");
//        System.out.println(JSON.toJSONString(redisTemplate.opsForZSet().range("myset",0,-1)));
//    }
//
//
//    @Test
//    public void testQueryCouponNotice(){
//        System.out.println("testQueryCouponNotice \n"+JSON.toJSON(couponService.queryCouponNotice()));
//    }
//
//    @Test
//    public void testUpdateCoupon(){
//        couponService.updateCoupon("1_101134");
//    }*/
//
//
//
//
//}

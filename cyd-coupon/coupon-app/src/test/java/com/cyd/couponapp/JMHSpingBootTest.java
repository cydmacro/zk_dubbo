//package com.cyd.couponapp;
//
//import com.cyd.couponapp.service.dubbo.CouponService;
//import org.openjdk.jmh.annotations.*;
//import org.openjdk.jmh.runner.Runner;
//import org.openjdk.jmh.runner.RunnerException;
//import org.openjdk.jmh.runner.options.Options;
//import org.openjdk.jmh.runner.options.OptionsBuilder;
//import org.springframework.boot.SpringApplication;
//import org.springframework.context.ConfigurableApplicationContext;
//
//@State(Scope.Thread)
//public class JMHSpingBootTest {
//
//    private  ConfigurableApplicationContext context;
//    private  CouponService couponService;
//
//    public static void main(String[] fsdf) throws RunnerException {
//        Options options=new OptionsBuilder().include(JMHSpingBootTest.class.getName()+"*")
//                .warmupIterations(2).measurementIterations(2)
//                .forks(1).build();
//        //不要忘记启动
//        new Runner(options).run();
//    }
//
//    /**
//     * SETUP 初始化执行一次
//     */
//    @Setup(Level.Trial)
//    public void init(){
//        String args="";
////      ApplicationContext context=new ClassPathXmlApplicationContext("classpath://*.xml");
//        context=SpringApplication.run(CouponAppApplication.class,args);
//        couponService=context.getBean(CouponService.class);
//    }
//
//
//    /**
//     * 数据库拿数据
//     */
//    @Benchmark
//    public void testDB(){
//        System.out.println(couponService.loadCoupon(1));
//    }
//
//    /**
//     * 缓存拿数据
//     */
//    @Benchmark
//    public void testCache(){
//        System.out.println(couponService.getCouponList());
//    }
//
//
//    /**
//     * map拿数据
//     */
//    @Benchmark
//    public void testMap(){
//        System.out.println(couponService.getCouponList4Map());
//    }
//
//   /* Benchmark                    Mode  Cnt      Score   Error  Units
//    JMHSpingBootTest.testCache  thrpt    2  31367.609          ops/s
//    JMHSpingBootTest.testDB     thrpt    2    829.311          ops/s
//    JMHSpingBootTest.testMap    thrpt    2  47268.246          ops/s*/
//
//
//}

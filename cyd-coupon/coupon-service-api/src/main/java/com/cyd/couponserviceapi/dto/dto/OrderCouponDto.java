package com.cyd.couponserviceapi.dto.dto;

public class OrderCouponDto {



    private  Integer orderId;

    private  String couponCode;

    private  Integer userId;

    private  Integer status; //0 支付成功（优惠券核销）  1 退款（状态未用） 2 下单成功

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status=status;
    }

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId=orderId;
    }

    public String getCouponCode() {
        return couponCode;
    }

    public void setCouponCode(String couponCode) {
        this.couponCode=couponCode;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId=userId;
    }


    @Override
    public String toString() {
        return "OrderCouponDto{" +
                "orderId='" + orderId + '\'' +
                ", couponCode='" + couponCode + '\'' +
                ", userId='" + userId + '\'' +
                ", status='" + status + '\'' +
                '}';
    }


    public OrderCouponDto(Integer orderId, String couponCode, Integer userId) {
        this.orderId=orderId;
        this.couponCode=couponCode;
        this.userId=userId;
        this.status=2;
    }



    public OrderCouponDto(Integer orderId, String couponCode, Integer userId, Integer status) {
        this.orderId=orderId;
        this.couponCode=couponCode;
        this.userId=userId;
        this.status=status;
    }



    public OrderCouponDto() {
    }

}

package com.cyd.couponserviceapi.dto;

import java.io.Serializable;

/**
 * 用户自己的优惠券
 */
public class UserCouponInfoDto  extends  UserCouponDto implements Serializable {

    public Integer getReduceAmount() {
        return reduceAmount;
    }

    public void setReduceAmount(Integer reduceAmount) {
        this.reduceAmount=reduceAmount;
    }

    public Integer getAchieveAmount() {
        return achieveAmount;
    }

    public void setAchieveAmount(Integer achieveAmount) {
        this.achieveAmount=achieveAmount;
    }
      //所减金额
    private Integer reduceAmount;

    //达到满减资格金额
    private Integer achieveAmount;
}

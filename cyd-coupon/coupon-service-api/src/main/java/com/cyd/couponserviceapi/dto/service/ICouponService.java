package com.cyd.couponserviceapi.dto.service;

import com.cyd.couponserviceapi.dto.CouponDto;
import com.cyd.couponserviceapi.dto.CouponNoticeDto;
import com.cyd.couponserviceapi.dto.UserCouponDto;
import com.cyd.couponserviceapi.dto.UserCouponInfoDto;

import java.util.List;

public  interface ICouponService  {

    /**
     * 获取优惠券列表
     * @auth cyd
     * @return
     */
    public  List<CouponDto> getCouponList();

    /**
     * 获取优惠券
     * @auth cyd
     * @return
     */
    public  String saveUserCoupon(UserCouponDto dto);

    /**
     * 获取用户自己的优惠券
     * @auth cyd
     * @return
     */
    public List<UserCouponInfoDto> userCouponList(Integer userId);


    /**
     * 公告栏
     */
    public List<CouponNoticeDto> queryCouponNotice();
}

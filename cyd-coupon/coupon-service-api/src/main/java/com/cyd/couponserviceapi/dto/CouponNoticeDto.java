package com.cyd.couponserviceapi.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * 公告栏
 */
public class CouponNoticeDto implements Serializable {
    private Integer id;

    private String code;

    private String picUrl;

    private Integer reduceAmount;

    private Integer achieveAmount;

    private Long stock;

    private String title;

    private Date createTime;

    private Integer status;

    private  Integer userId;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId=userId;
    }




    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getPicUrl() {
        return picUrl;
    }

    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl;
    }

    public Integer getReduceAmount() {
        return reduceAmount;
    }

    public void setReduceAmount(Integer reduceAmount) {
        this.reduceAmount = reduceAmount;
    }

    public Integer getAchieveAmount() {
        return achieveAmount;
    }

    public void setAchieveAmount(Integer achieveAmount) {
        this.achieveAmount = achieveAmount;
    }

    public Long getStock() {
        return stock;
    }

    public void setStock(Long stock) {
        this.stock = stock;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }


    @Override
    public String toString() {
        return "CouponNoticeDto{" +
                "id=" + id +
                ", code='" + code + '\'' +
                ", picUrl='" + picUrl + '\'' +
                ", reduceAmount=" + reduceAmount +
                ", achieveAmount=" + achieveAmount +
                ", stock=" + stock +
                ", title='" + title + '\'' +
                ", createTime=" + createTime +
                ", status=" + status +
                ", userId=" + userId +
                '}';
    }

}




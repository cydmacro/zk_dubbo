package com.cyd.couponserviceapi.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * 用户优惠券
 */
public class UserCouponDto  implements Serializable {

    private Integer couponId;

    public Integer getCouponId() {
        return couponId;
    }

    public void setCouponId(Integer couponId) {
        this.couponId=couponId;
    }

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId=orderId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId=userId;
    }

    private Integer orderId;

    private Integer userId;


    public String getUserCouponCode() {
        return userCouponCode;
    }

    public void setUserCouponCode(String userCouponCode) {
        this.userCouponCode=userCouponCode;
    }

    private  String userCouponCode;




}
